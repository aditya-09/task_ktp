package org.example;

import org.example.models.InsertUser;
import org.example.models.ShowUser;
import org.example.models.ShowWhereUser;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.sql.SQLException;

/**
 * A simple WebSocketServer implementation. Keeps track of a "chatroom".
 */
public class Server extends WebSocketServer {

    Integer id;
    Integer nik;
    String nama;
    String jk;
    String agama;
    String kwr;

    public Server(int port ) throws UnknownHostException {
        super( new InetSocketAddress( port ) );
    }

//    public ChatServer(InetSocketAddress address ) {
//        super( address );
//    }

    @Override
    public void onOpen( WebSocket conn, ClientHandshake handshake ) {
        System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " entered the room!" );
        ShowUser s = new ShowUser();
        for(String data : s.getJSON()){
            broadcast(data);
        }
    }

    @Override
    public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
        broadcast( conn + " has left the room!" );
        System.out.println( conn + " has left the room!" );
    }

    @Override
    public void onMessage( WebSocket conn, String message ) {
        System.out.println( conn + ": " + message );
        JSONObject myResp = new JSONObject((message));
//        ShowUser s = new ShowUser();
//        for(String data : s.getJSON()){
//            broadcast(data);
//        }
//        if (myResp.getString("data") == "tos"){
//
//        }
        this.nik = myResp.getInt("nik");
        this.nama = myResp.getString("nama");
        this.jk = myResp.getString("jk");
        this.agama = myResp.getString("agama");
        this.kwr = myResp.getString("kwr");

        InsertUser m = new InsertUser();
        m.insertData(this.nik, this.nama, this.jk, this.agama, this.kwr);


//        ShowWhereUser sw = new ShowWhereUser();
//        sw.showWhereUser(this.nik);
//        JSONObject data = sw.getJson();
//        System.out.println(data);
//        broadcast(String.valueOf(data));

    }

//    @Override
//    public void onMessage( WebSocket conn, ByteBuffer message ) {
//        broadcast( message.array() );
//        System.out.println( conn + ": " + message );
//    }


    public static void main( String[] args ) throws InterruptedException , IOException {
        int port = 3132;
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }
        Server s = new Server( port );
        s.start();
        System.out.println( "ChatServer started on port: " + s.getPort() );

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true ) {
            String in = sysin.readLine();
            s.broadcast( in );
            if( in.equals( "exit" ) ) {
                s.stop(1000);
                break;
            }
        }
    }
    @Override
    public void onError( WebSocket conn, Exception ex ) {
        ex.printStackTrace();
        if( conn != null ) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        System.out.println("Server started!");
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);
    }

}