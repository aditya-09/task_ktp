package org.example.models;

import org.json.JSONObject;

import java.sql.*;
import java.util.Scanner;

public class InsertUser {
    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_ktp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    //    dibutuhkan

    public void insertData(Integer nik, String nama, String jk, String agama, String kwr){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String insertSQL = "INSERT INTO user (nik, nama, jk, agama, kwr) VALUES ("+nik+",\""+nama+"\",\""+jk+"\",\""+agama+"\",\""+kwr+"\")";
            stmt.executeUpdate(insertSQL);
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        InsertUser m = new InsertUser();
//        m.insertData(1131293, "ate", "laki-laki", "islam", "WNI");
//    }
}
