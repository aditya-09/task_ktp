package org.example.models;

import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ShowWhereUser {
    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_ktp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    //    dibutuhkan
    Integer id;
    Integer nik;
    String nama;
    String jk;
    String agama;
    String kwr;
    String jsonString;
    JSONObject json_data;

    public void showWhereUser(Integer nik){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "SELECT * FROM user WHERE nik=%d";
            sql = String.format(sql, nik);
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                System.out.println("NIK \t: " + rs.getInt("nik"));
                this.id = rs.getInt("id");
                this.nik = rs.getInt("nik");
                this.nama = rs.getString("nama");
                this.jk = rs.getString("jk");
                this.agama = rs.getString("agama");
                this.kwr = rs.getString("kwr");
                this.jsonString= "{\"id\" : "+id+",\"nik\":"+nik+",\"nama\":\""+nama+"\",\"jk\":\""+jk+"\",\"agama\":\""+agama+"\",\"kwr\":\""+kwr+"\"}";
            }
            this.json_data= new JSONObject((this.jsonString));
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJson() {
        return this.json_data;
    }

    public static void main(String[] args) {
        ShowWhereUser s = new ShowWhereUser();
        s.showWhereUser(1293);
        System.out.println(s.getJson());
    }
}
