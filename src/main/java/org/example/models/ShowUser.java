package org.example.models;

import org.json.JSONObject;

import javax.json.JsonArray;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ShowUser {
    // Menyiapkan paramter JDBC untuk koneksi ke datbase
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/java_ktp?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "";

    // Menyiapkan objek yang diperlukan untuk mengelola database
    static Connection conn;
    static Statement stmt;
    static ResultSet rs;

    //    dibutuhkan
    Integer id;
    Integer nik;
    String name;
    String jk;
    String agama;
    String kwr;
    ArrayList <String> json_string = new ArrayList<String>();
    JSONObject json_data;
    JsonArray json_array;

    public void showUser(){
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = conn.createStatement();

            String sql = "SELECT * FROM user";
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                this.id = rs.getInt("id");
                this.nik = rs.getInt("nik");
                this.name = rs.getString("nama");
                this.jk = rs.getString("jk");
                this.agama = rs.getString("agama");
                this.kwr = rs.getString("kwr");
                this.json_string.add("{\"id\" : "+id+",\"nik\":"+nik+",\"nama\":\""+this.name+"\",\"jk\":\""+this.jk+"\",\"agama\":\""+this.agama+"\", \"kwr\" : \""+this.kwr+"\" }");
            }
//            System.out.println(this.json_string);
//            JSONArray jsArray2 = new JSONArray(this.json_string);
//            System.out.println(jsArray2);
            stmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getJSON(){
        showUser();
        return this.json_string;
    }

//    public static void main(String[] args) {
//        ShowUser s = new ShowUser();
//        s.showUser();
//        System.out.println(s.getJSON());
//    }
}
